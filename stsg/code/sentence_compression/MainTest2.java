package stsg.code.sentence_compression;

import java.util.ArrayList;
import java.util.Scanner;

import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import stsg.code.util.Node;
import stsg.code.util.OutputResult;
import stsg.code.util.Rule;
import stsg.code.util.RuleParser;
import stsg.code.util.TreeParser;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

public class MainTest2 {
	public static void main(String[] args) {
		// 初始化ansj切词工具、斯坦福语法树工具
		ToAnalysis.parse("");
		StanfordParser.lp = LexicalizedParser
				.loadModel("lib/chinesePCFG.ser.gz");
		System.out.println("comp-user input:");

		Scanner input = new Scanner(System.in);

		while (true) {
			String stri = input.nextLine();
			if (stri.trim().length() == 0) {
				break;
			}
			String[] strs = stri.split("，|。");
			for (int i = 0; i < strs.length; i++) {
				strs[i] += ".";
			}
			int cnti = 0;

			while (true) {
				// 开始缩句
				if (cnti == strs.length) {
					break;
				}
				String str = strs[cnti++];
				java.util.List<Term> sparse = ToAnalysis.parse(str);
				String[] source = new String[sparse.size()]; // 原句分词String[]
				for (int i = 0; i < source.length; i++) {
					source[i] = sparse.get(i).getName();
				}
				Node sroot = StanfordParser.createTree(source); // 原句建树
				// System.out.println(StanfordParser.printTree(sroot));
				// 取到原句建树中的所有结点Node
				TreeParser facility = new TreeParser(sroot);
				ArrayList<Node> allNodes = facility.list;
				// 开始比对规则
				int cnt = 0;
				while (true) {
					boolean canExit = true;
					for (int i = 0; i < allNodes.size() - source.length; i++) {
						// 导入规则
						MakeRulesFromCorpus.rulesTreeList.clear();
						MakeRulesFromCorpus.ImportRules();
						ArrayList<Rule> rlist = MakeRulesFromCorpus.rulesTreeList;
						for (Rule r : rlist) {
							if (RuleParser.isEqualTree(allNodes.get(i),
									r.source)) {
								facility.Visit(allNodes.get(i), r.source);
								facility.Visit_Target(r.target);
								allNodes.get(i).replace(r.target);
								// System.out.println(StanfordParser
								// .printTree(r.source));
								canExit = false;
								break;
							}
						}
					}
					if (canExit) {
						break;
					}
					cnt++;
					if (cnt >= 100) {
						System.out.println("Wrong!");
						break;
					}
				}
				// System.out.println(StanfordParser.printTree(sroot));
				OutputResult outans = new OutputResult();
				outans.visit(sroot);
				String output_keyven = outans.toString();
				if (cnti == strs.length) {
					System.out.println(output_keyven);
				} else {
					System.out.print(output_keyven + " ");
				}
			}
			System.out.println();
		}
		input.close();
	}
}
