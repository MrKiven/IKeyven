```java
IKeyven使用说明

在_Keyven/hankcs.txt中输入所要测试的文本
运行_Keyven/MainTest1.java

boolean  choice  =  false;
for  (String  str  :  list)  {
        String  oris  =  str;
        System.out.print(str  +  "\t");
        ISTSG  user  =  STSGFactory.stsgproducer(STSGType.COMPRESSION);          
        //  同步树替换文法调用接口
        str  =  user.synchronous_tree_substitution_grammar(str);
        UserDefine.addtemps();
        //  在UserDefine.java中定义“有没有”、“是不是”之类的词
        Dependency  temp  =  new  Dependency();
        //  防止分词结果：[有/没有]依赖树直接提取“有”作为谓词
        DepObject  obj  =  temp.process(str,  choice);
        if  (obj.isUseless())  {
                String  ans  =  Dependency.repair(oris,  str);
                System.out.println(ans);
        }  else  {
                String  ans  =  Dependency.repair(oris,  obj.toString());
                if  (Dependency.isCC(obj,  str))  {
                        System.out.println(str);
                }  else  {
                        System.out.println(ans);
                }
        }
        UserDefine.removetemps();        
        //  复原“有没有”、“是不是”之类的自定义词，以免对STSG造成影响
};
```
```
我一直很喜欢你                              我喜欢你    
我被你喜欢着    							你喜欢我								
美丽又善良的你被卑微的我深深地喜欢着……		我喜欢你								
有自信的程序员才能把握未来					程序员把握未来								
主干识别可以提高检索系统的智能				主干识别提高智能								
这个项目的作者是hankcs					    作者是hankcs								
hankcs是一个无门无派的浪人					hankcs是浪人								
搜索hankcs可以找到我的博客					搜索hankcs找到博客								
静安区体育局2013年部门决算情况说明表		静安区体育局2013年部门决算情况说明表																
我是一个有趣的中国人						我是中国人								
我是一个中国人有趣的						我是中国人								
三星有什么好推荐的							什么有推荐的								
你把我打败了								你打败我								
这个idea来源于hankcs						idea来源于hankcs								
这个算法结合STSG更好						算法结合stsg更好								
算法的未知因素太多							未知因素多								
偶尔我喜欢一个人静静地听歌					我喜欢歌								
这是一款精致的灯							这是灯								
它拥有高雅的外观以及美丽的造型				它拥有外观以及造型																
夜晚的月亮是多么清澈。						月亮是清澈								
朝着这个方向								朝着方向								
小鸟慢慢地跳过来							小鸟跳过来								
它的叫声是多么可爱							叫声是可爱																
都是纯真的记忆。							都是记忆。								
到了一个成熟的阶段							到了阶段								
喜欢选择性结交朋友							选择性喜欢朋友								
真正的境界								    境界								
就是能懂得取舍								就是懂得取舍								
让生命的本质回归							让本质回归								
那样我们的心境真正开朗						那样心境开朗								
回复一颗柔软的平常心						回复平常心																
只管让心里装满阳光							只管让阳光								
眼中的世界								    世界								
他是善良的勤劳的工人						他是工人								
我记得那个小男孩非常帅气					我记得男孩								
一个漫长的故事								故事								
我的未来不是梦								未来不是梦								
我的心跟着希望在动							心跟着希望在动								
投资要求期望收益一定是大于0的				期望收益是大于的								
所以赌博主要是侥幸心理在起作用				赌博起作用								
赌博的心态在投资中经常出现					心态在投资中经常出现
它主要依靠市场价格的预测					它依靠预测								
美国国内生产总值的增长非常强劲				美国国内生产总值的的增长强劲								
需求的力量是一种伟大的力量					力量是力量								
谭慧在北京一家有名的婚纱店拍了一套婚纱照	谭慧拍婚纱照								
在自己生活的城市我们被人欺诈				人欺诈我们								
买房是身心与经济的负担						买房是负担								
我们要不再给资本家当炮灰					我们给资本家当炮灰								
垄断者是产品的唯一卖家						垄断者是卖家								
```